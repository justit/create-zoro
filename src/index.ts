#! /usr/bin/env node
import path from 'path'
import { program } from 'commander'
import help from './core/help'
import { isExist, checkDeployConfig, checkConfigScheme } from './utils/format'
import init from './init'
import select from './select'
import deploy from './core/deploy'

// help处理
help()

// 配置文件所在目录
const targetDir = path.join(process.cwd(), 'zoro')
// 配置文件路径
const targetPath = path.join(targetDir, 'index.jsonc')

/**
 * 执行 zoro
 * 判断是否有配置文件
 *  没有
 *    生成配置文件
 *  有
 *    命令行选中部署配置
 */
program
  .action((name, { args }) => {
    if (args.length) {
      program.outputHelp()
      return
    }
    // 判断配置文件是否存在
    if (isExist(targetPath)) {
      select(targetPath)
    } else {
      init(targetDir, targetPath)
    }
  })

/**
 * 执行 zoro init/i
 * 生成配置文件
 */
program
  .command('init')
  .alias('i')
  .description('初始化配置信息')
  .action(() => init(targetDir, targetPath))

/**
 * 有参数直接执行部署
 * 当有[versionOptions]参数时 不执行部署
 * 如: zoro dev
 */
const versionOptions = ['-V', '--version', 'i', 'init']
const arg = process.argv.slice(2)[0]
if (arg && !versionOptions.includes(arg) && isExist(targetPath)) {
  createProgram()
}

function createProgram () {
  // 检测部署配置是否合理
  const deployConfigs = checkDeployConfig(targetPath)
  if (!deployConfigs.length) {
    return
  }

  // 注册部署命令
  deployConfigs.forEach(config => {
    const { command, projectName, name } = config
    program
      .command(`${command}`)
      .description(`将${(projectName)}项目部署至${(name)}`)
      .action(() => {
        checkConfigScheme(config)
        deploy(config)
      })
  })
}

program.parse()
