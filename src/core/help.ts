import { program } from 'commander'
import { version } from '../../package.json'

export default () => {
  program.version(version)
  program.option('i init', '初始化配置信息')
  program.option('[command]', '部署命令')
}
