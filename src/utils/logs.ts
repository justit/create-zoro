import { magenta, blue, green, red, underline } from 'kolorist'
// 开始部署日志
export const startLog = function (content: string) {
  console.log(magenta(content))
}

// 信息日志
export const infoLog = function (content: string) {
  console.log(blue(content))
}

// 成功日志
export const successLog = function (content: string) {
  console.log(green(content))
}

// 错误日志
export const errorLog = function (content: string) {
  console.log(red(content))
}

// 划线
export const underlineLog = function (content: string) {
  return underline(content)
}
