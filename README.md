# create-zoro

> 罗罗诺亚·索隆，是草帽一伙的战斗员，是使用三把刀战斗的三刀流剑士。立志成为世界第一大剑豪。

—— 来自 [海贼王](https://one-piece.com/) 。

<img src="http://img.ldjwl.com/images/20220812/69395756823a416b8cc006c7cdfc791e.jpg" width="474" height="266" />


啊，不。

这只是一个自动化部署的包...

---

### 食用方式
```
$ npm install create-zoro -g
```

#### 在需要部署的项目下运行

```
zoro
```

> 首次运行会在项目根目录下生成配置信息 **zoro/index.json**
> 或者执行 zoro init 生成配置信息
```
{
  "privateKeyPath": "", // 本地私钥地址，位置一般在C:/Users/xxx/.ssh/id_rsa，非必填，有私钥则配置
  "passphrase": "", // 本地私钥密码，非必填，有私钥则配置

  // 根据需要进行配置，如只需部署prod线上环境，请删除dev测试环境配置，反之亦然，支持多环境部署
  "dev": {
    "name": "测试环境", // 环境名称
    "script": "npm run build", // 打包脚本
    "host": "", // 测试服务器地址
    "port": 22, // ssh port，一般默认22
    "username": "", // 登录服务器用户名
    "password": "", // 登录服务器密码
    "distPath": "dist", // 本地打包后端目录名
    "webDir": "", // 测试环境web目录
  },
  "prod": {
    "name": "线上环境", // 环境名称
    "script": "npm run build", // 打包脚本
    "host": "", // 线上服务器地址
    "port": 22, // ssh port，一般默认22
    "username": "", // 登录服务器用户名
    "password": "", // 登录服务器密码
    "distPath": "dist", // 本地打包后端目录名
    "webDir": "" // 线上环境web目录
  }
  // 再还有多余的环境按照这个格式写即可
}
```
#### 执行部署
```
// 根据配置信息选择需要部署的环境
zoro <command>
```
然后，等待部署完成。。。

#### 部署步骤
- 第一步，执行打包脚本
- 第二步，将脚本打包成zip
- 第三步，连接SSH
- 第四步，备份远端项目
- 第五步，上传zip包
- 第六步，在服务解压并删除zip包
- 第七步，删除本地dist.zip包

> **兼容性注意:**
> 要求 [Node.js](https://nodejs.org/en/) 版本 >=14.6.0. 当你的包管理器发出警告时，请注意升级你的 Node 版本。
